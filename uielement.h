#ifndef UIELEMENT_H_INCLUDED
#define UIELEMENT_H_INCLUDED


class UiElement
{
public:
	virtual void enable() = 0;
	virtual void disable() = 0;
};


#endif // UIELEMENT_H_INCLUDED
