#include "buzzer.h"


Buzzer::Buzzer(DBus::Connection &c, ccaux::Buzzer &b)
: DBus::ObjectAdaptor(c, std::string("/org/pragmatux/DisplayHal/Buzzer"))
, buzzer(b)
, enabled(true)
{
}


void Buzzer::On()
{
	if (enabled)
		buzzer.setTrigger(true);
}


void Buzzer::Off()
{
	buzzer.setTrigger(false);
}


void Buzzer::enable()
{
	enabled = true;
}

void Buzzer::disable()
{
	enabled = false;
	Off();
}
