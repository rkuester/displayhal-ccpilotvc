#include "keyswitch.h"
#include "include-glibmm.h"


Keyswitch::Keyswitch(DBus::Connection &dbus, ccaux::Config &c)
: DBus::ObjectAdaptor(dbus, "/org/pragmatux/DisplayHal/Keyswitch")
, config(c)
, is_on(false)
{
	poller = Glib::signal_timeout().connect_seconds(
		[this](){poll(); return true;},
		1 /*s*/);
}


Keyswitch::~Keyswitch()
{
	if (poller)
		poller.disconnect();
}


bool Keyswitch::State()
{
	return is_on;
}


void Keyswitch::poll()
{
	bool state = config.getOnOffSignalState();

	if (is_on != state) {
		is_on = state;
		StateChanged(is_on);
	}
}
