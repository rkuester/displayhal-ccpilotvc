#include "userinterface.h"
#include "uielement.h"


UserInterface::UserInterface(DBus::Connection &dbus)
: DBus::ObjectAdaptor(dbus, "/org/pragmatux/DisplayHal/UserInterface")
{
	Enabled = true;
}


void UserInterface::add_element(UiElement &e)
{
	elements.push_back(e);
}


void UserInterface::
on_set_property(DBus::InterfaceAdaptor &i, const std::string &property, const DBus::Variant &enabled)
{
	if (property != "Enabled")
		throw DBus::ErrorInvalidArgs("property not equal to \"Value\"");

	if (enabled)
		for (UiElement &e : elements)
			e.enable();
	else
		for (auto e = elements.rbegin(); e != elements.rend(); ++e)
			e->get().disable();
}
