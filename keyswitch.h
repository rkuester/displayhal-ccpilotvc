#ifndef KEYSWITCH_H_INCLUDED
#define KEYSWITCH_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal.Keyswitch1-adaptor.h"
#include "cc-aux.h"
#include <sigc++/connection.h>

class Keyswitch
: public org::pragmatux::DisplayHal::Keyswitch1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	Keyswitch(DBus::Connection&, ccaux::Config&);
	~Keyswitch();

  private:
	bool State();

	bool is_on;
	void poll();
	ccaux::Config &config;
	sigc::connection poller;
};


#endif // KEYSWITCH_H_INCLUDED
