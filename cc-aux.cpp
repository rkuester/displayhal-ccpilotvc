#include "cc-aux.h"
#include <sstream>
#include <vector>

using namespace ccaux;


Err::Err(const CrossControl::eErr &e)
: code(e)
{
}


const char* Err::what() const throw()
{
	return CrossControl::GetErrorStringA(code);
}


void check(const CrossControl::eErr& e)
{
	if (e != CrossControl::ERR_SUCCESS)
		throw Err(e);
}


About::About()
: handle(CrossControl::GetAbout())
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);
}

About::~About()
{
	if (handle)
		CrossControl::About_release(handle);
}


std::string About::getMainProdArtNr()
{
	std::vector<char> buf(40);
	auto code = CrossControl::About_getMainProdArtNr(handle, buf.data(), buf.capacity());
	check(code);
	return std::string(buf.data());
}


std::string About::getMainProdRev()
{
	std::vector<char> buf(40);
	auto code = CrossControl::About_getMainProdRev(handle, buf.data(), buf.capacity());
	check(code);
	return std::string(buf.data());
}


std::string About::getMainPCBSerial()
{
	std::vector<char> buf(40);
	auto code = CrossControl::About_getMainPCBSerial(handle, buf.data(), buf.capacity());
	check(code);
	return std::string(buf.data());
}


AuxVersion::AuxVersion()
: handle(CrossControl::GetAuxVersion())
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);
}


AuxVersion::~AuxVersion()
{
	if (handle)
		CrossControl::AuxVersion_release(handle);
}


std::string AuxVersion::getSSVersion()
{
	unsigned char major = 0;
	unsigned char minor = 0;
	unsigned char release = 0;
	unsigned char build = 0;

	auto code = CrossControl::AuxVersion_getSSVersion(
		handle,
		&major,
		&minor,
		&release,
		&build);
	check(code);

	std::stringstream s;
	s << static_cast<int>(major) << "."
	  << static_cast<int>(minor) << "."
	  << static_cast<int>(release) << "."
	  << static_cast<int>(build);

	return s.str();
}


Backlight::Backlight()
: handle(CrossControl::GetBacklight())
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);
}


Backlight::~Backlight()
{
	if (handle)
		CrossControl::Backlight_release(handle);
}


Backlight::Intensity Backlight::getIntensity()
{
	Intensity i;
	auto code = CrossControl::Backlight_getIntensity(handle, &i);
	check(code);
	return i;
}


void Backlight::setIntensity(Intensity i)
{
	auto code = CrossControl::Backlight_setIntensity(handle, i);
	check(code);
}


Buzzer::Buzzer()
: handle(CrossControl::GetBuzzer())
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);

	setFrequency(1000 /*Hz*/);
	setVolume(50);
}


Buzzer::~Buzzer()
{
	if (handle)
		CrossControl::Buzzer_release(handle);
}


void Buzzer::buzze(Milliseconds ms)
{
	auto code = CrossControl::Buzzer_buzze(handle, ms, false);
	check(code);
}


void Buzzer::setFrequency(Hertz hz)
{
	auto code = CrossControl::Buzzer_setFrequency(handle, hz);
	check(code);
}


void Buzzer::setTrigger(bool on)
{
	auto code = CrossControl::Buzzer_setTrigger(handle, on);
	check(code);
}


void Buzzer::setVolume(Volume v)
{
	auto code = CrossControl::Buzzer_setVolume(handle, v);
	check(code);
}


FrontLed::FrontLed()
: handle(CrossControl::GetFrontLED())
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);
}


FrontLed::~FrontLed()
{
	if (handle)
		CrossControl::FrontLED_release(handle);
}


void FrontLed::setSignal(const Frequency &f, const DutyCycle &d)
{
	auto code = CrossControl::FrontLED_setSignal(handle, f, d);
	check(code);
}


void FrontLed::setIntensity(Intensity i)
{
	auto code = CrossControl::FrontLED_setColor(handle, 0, 0, i);
	check(code);
}


CfgIn::CfgIn(Channel c)
: handle(CrossControl::GetCfgIn())
, channel(c)
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);

	auto code = CrossControl::CfgIn_setCfgInMode(
			handle, c, CrossControl::CFGIN_LOW_SWITCH);
	check(code);
}


CfgIn::~CfgIn()
{
	if (handle)
		CrossControl::CfgIn_release(handle);
}


bool CfgIn::getValue_isHigh()
{
	short unsigned value = 0;

	auto code = CrossControl::CfgIn_getValue(handle, channel, &value);
	check(code);

	if (value == 0 || value == 1)
		return value == 1;
	else
		throw std::out_of_range("bad value read from digital input");
}


Config::Config()
: handle{CrossControl::GetConfig()}
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);
}


Config::~Config()
{
	if (handle)
		CrossControl::Config_release(handle);
}


void Config::setOnOffSigAction(PowerAction p)
{
	CrossControl::PowerAction action;

	switch (p) {
	case (PowerAction::ActionSuspend):
		action = CrossControl::ActionSuspend;
		break;
	case (PowerAction::ActionShutDown):
		action = CrossControl::ActionShutDown;
		break;
	case (PowerAction::NoAction):
		action = CrossControl::NoAction;
		break;
	default:
		throw std::out_of_range("invalid on-off signal action");
	}

	auto code = CrossControl::Config_setOnOffSigAction(handle, action);
	check(code);
}


void Config::setPowerOnStartup(bool enable)
{
	CrossControl::CCStatus status =
		enable ? CrossControl::Enabled : CrossControl::Disabled;
	auto code = CrossControl::Config_setPowerOnStartup(handle, status);
	check(code);
}


bool Config::getOnOffSignalState()
{
	CrossControl::CCStatus status;

	auto code = CrossControl::Config_getOnOffSignalState(handle, &status);
	check(code);

	return status == CrossControl::Enabled;
}


PWMOut::PWMOut(const Channel c)
: handle{CrossControl::GetPWMOut()}
, channel{c}
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);

	setPWMOutOff();
}


PWMOut::~PWMOut()
{
	if (handle)
		CrossControl::PWMOut_release(handle);
}


void PWMOut::setPWMOutOff()
{
	auto code = CrossControl::PWMOut_setPWMOutOff(handle, channel);
	check(code);
}


void PWMOut::setPWMOutputChannelDutyCycle(const DutyCycle duty)
{
	if (duty < 0 || duty > 100)
		throw std::out_of_range("bad duty cycle set to digital output");

	auto code = CrossControl::PWMOut_setPWMOutputChannelDutyCycle(handle, channel, duty);
	check(code);
}


PWMOut::DutyCycle PWMOut::getPWMOutputChannelDutyCycle()
{
	DutyCycle duty;

	auto code = CrossControl::PWMOut_getPWMOutputChannelDutyCycle(handle, channel, &duty);
	check(code);

	return duty;
}


Lightsensor::Lightsensor()
: handle(CrossControl::GetLightsensor())
{
	if (handle == NULL)
		throw Err(CrossControl::ERR_MEM_ALLOC_FAIL);
}

Lightsensor::~Lightsensor()
{
	if (handle)
		CrossControl::Lightsensor_release(handle);
}


unsigned Lightsensor::getIlluminance()
{
	unsigned short value;

	auto code = CrossControl::Lightsensor_getIlluminance(handle, &value);
	check(code);

	return value;
}


void Lightsensor::setOperatingRange(Lightsensor::Range r)
{
	CrossControl::LightSensorOperationRange range;

	switch (r) {
	case (Range::Lux_1000):
		range = CrossControl::RangeStandard;
		break;
	case (Range::Lux_4000):
		range = CrossControl::RangeExtended;
		break;
	default:
		throw std::out_of_range("invalid lightsensor range");
	}

	auto code = CrossControl::Lightsensor_setOperatingRange(handle, range);
	check(code);
}


void PWMOut::setPWMOutputChannelFrequency(const Frequency freq)
{
	auto code = CrossControl::PWMOut_setPWMOutputChannelFrequency(handle, channel, freq);
	check(code);
}
