#ifndef DIGITALINPUT_H_INCLUDED
#define DIGITALINPUT_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal.DigitalInput1-adaptor.h"
#include "cc-aux.h"
#include <sigc++/connection.h>


class DigitalInput
: public org::pragmatux::DisplayHal::DigitalInput1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	DigitalInput(DBus::Connection&, ccaux::CfgIn&, const std::string name);
	~DigitalInput();

  private:
	void Subscribe(const unsigned &flags);
	void poll();
	ccaux::CfgIn &input;
	sigc::connection poller;
};


#endif // DIGITALINPUT_H_INCLUDED
