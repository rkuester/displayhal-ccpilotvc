#ifndef DIGITALOUTPUT_H_INCLUDED
#define DIGITALOUTPUT_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal.DigitalOutput1-adaptor.h"
#include "cc-aux.h"


class DigitalOutput
: public org::pragmatux::DisplayHal::DigitalOutput1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	DigitalOutput(DBus::Connection&, ccaux::PWMOut&, const std::string name);

  private:
	void on_set_property(DBus::InterfaceAdaptor &, const std::string &, const DBus::Variant &);
	void on();
	void off();
	ccaux::PWMOut &output;
};


#endif // DIGITALOUTPUT_H_INCLUDED
