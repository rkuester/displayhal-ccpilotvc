#include "digitaloutput.h"


DigitalOutput::DigitalOutput(DBus::Connection &c, ccaux::PWMOut &out, const std::string name)
: DBus::ObjectAdaptor(c, std::string("/org/pragmatux/DisplayHal/") + name)
, output(out)
{
	Value = (output.getPWMOutputChannelDutyCycle() > 0) ? 1 : 0;
}


void DigitalOutput::
on_set_property(DBus::InterfaceAdaptor &i, const std::string &property, const DBus::Variant &value)
{
	if (property != "Value")
		throw DBus::ErrorInvalidArgs("property not equal to \"Value\"");

	const unsigned v = value;
	switch (v) {
	case 0:
		off();
		break;
	case 1:
		on();
		break;
	default:
		throw DBus::ErrorInvalidArgs("value must be 0 or 1");
	}

}


void DigitalOutput::on()
{
	output.setPWMOutputChannelDutyCycle(100);
	output.setPWMOutputChannelFrequency(1);
}


void DigitalOutput::off()
{
	output.setPWMOutOff();
}
