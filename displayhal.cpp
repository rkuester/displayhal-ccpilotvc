#include "displayhal.h"
#include <iostream>
#include "include-glibmm.h"


DisplayHal::DisplayHal(DBus::Connection &c)
: DBus::ObjectAdaptor(c, "/org/pragmatux/DisplayHal")
{
	PlatformIdRaw = about.getMainProdArtNr() + "_" + about.getMainProdRev();
	PlatformId = "cmd5_1";
	DeviceSerial = about.getMainPCBSerial();
	FirmwareInfo = "SS v" + auxversion.getSSVersion();
	BootloaderInfo = "U-Boot vUnknown";
}
