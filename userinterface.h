#ifndef USERINTERFACE_H_INCLUDED
#define USERINTERFACE_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal.UserInterface1-adaptor.h"
#include <vector>
#include <functional>
class UiElement;


class UserInterface
: public org::pragmatux::DisplayHal::UserInterface1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	UserInterface(DBus::Connection&);
	void add_element(UiElement &);
		// Add elements in the order in which they are enabled. They
		// will be disabled in the reverse order.

  private:
	void on_set_property(DBus::InterfaceAdaptor&, const std::string&, const DBus::Variant&);
	std::vector<std::reference_wrapper<UiElement>> elements;
};


#endif // USERINTERFACE_H_INCLUDED
