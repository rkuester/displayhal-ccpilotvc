/*
 *  Interface for the libcc-aux C++ wrapper
 */

#ifndef CC_AUX_H_INCLUDED
#define CC_AUX_H_INCLUDED


#include <cc-aux/CCAuxTypes.h>
#include <cc-aux/About.h>
#include <cc-aux/AuxVersion.h>
#include <cc-aux/Backlight.h>
#include <cc-aux/Buzzer.h>
#include <cc-aux/CfgIn.h>
#include <cc-aux/Config.h>
#include <cc-aux/FrontLED.h>
#include <cc-aux/Lightsensor.h>
#include <cc-aux/PWMOut.h>
#include <string>
#include <stdexcept>


namespace ccaux {


class Err : public std::exception {
public:
	Err(const CrossControl::eErr&);
	const char* what() const throw();

private:
	CrossControl::eErr code;
};


class About {
public:
	About();
	~About();
	std::string getMainProdArtNr();
	std::string getMainProdRev();
	std::string getMainPCBSerial();

private:
	CrossControl::ABOUTHANDLE handle;
};


class AuxVersion {
public:
	AuxVersion();
	~AuxVersion();

	std::string getSSVersion();

private:
	CrossControl::AUXVERSIONHANDLE handle;
};


class Buzzer {
public:
	Buzzer();
	~Buzzer();

	typedef int Milliseconds;
	typedef unsigned short Hertz; /* 700--10000 Hz */
	typedef unsigned short Volume; /* 0--51 */

	void buzze(Milliseconds);
	void setFrequency(Hertz);
	void setTrigger(bool on);
	void setVolume(Volume);

private:
	CrossControl::BUZZERHANDLE handle;
};


class Backlight {
public:
	typedef unsigned char Intensity;

	Backlight();
	~Backlight();
	Intensity getIntensity();
	void setIntensity(Intensity);

private:
	CrossControl::BACKLIGHTHANDLE handle;
};


class Config {
public:
	Config();
	~Config();

	enum class PowerAction {
		ActionSuspend,
		ActionShutDown,
		NoAction
	};

	void setOnOffSigAction(PowerAction);
	void setPowerOnStartup(bool);
	bool getOnOffSignalState();

private:
	CrossControl::CONFIGHANDLE handle;
};


class FrontLed {
public:
	typedef double Frequency;
	typedef unsigned char DutyCycle;
	typedef unsigned Intensity;
	static constexpr Intensity MinIntensity = 0;
	static constexpr Intensity MaxIntensity = 15;

	FrontLed();
	~FrontLed();
	void setSignal(const Frequency&, const DutyCycle&);
	void setIntensity(Intensity);

private:
	CrossControl::FRONTLEDHANDLE handle;
};


class CfgIn {
/* These configurable inputs are fixed in the 10k-ohm pull-down mode,
   assuming switched-to-ground inputs. Additional modes are available in the
   underlying hardware, and could be made configurable via modification of the
   library. */
public:
	typedef unsigned char Channel;

	CfgIn(const Channel);
	~CfgIn();
	bool getValue_isHigh();

private:
	CrossControl::CFGINHANDLE handle;
	Channel channel;
};


class Lightsensor {
public:
	enum class Range {Lux_1000, Lux_4000};

	Lightsensor();
	~Lightsensor();

	unsigned getIlluminance();
	void setOperatingRange(Range);

private:
	CrossControl::LIGHTSENSORHANDLE handle;
};


class PWMOut {
public:
	typedef unsigned char Channel;
	typedef double Frequency;
	typedef unsigned char DutyCycle;

	PWMOut(const Channel);
	~PWMOut();
	void setPWMOutOff();
	void setPWMOutputChannelDutyCycle(const DutyCycle);
	DutyCycle getPWMOutputChannelDutyCycle();
	void setPWMOutputChannelFrequency(const Frequency);

private:
	CrossControl::PWMOUTHANDLE handle;
	Channel channel;
};


} // end namespace ccaux


#endif // CC_AUX_H_INCLUDED
