#include "digitalinput.h"
#include <string>
#include <stdexcept>
#include "include-glibmm.h"


DigitalInput::DigitalInput(DBus::Connection &c, ccaux::CfgIn &in, const std::string name)
: DBus::ObjectAdaptor(c, std::string("/org/pragmatux/DisplayHal/") + name)
, input(in)
{
	Value = 0;
}


DigitalInput::~DigitalInput()
{
	if (poller)
		poller.disconnect();
}


void DigitalInput::Subscribe(const unsigned &flag)
{
	if (!poller) {
		poll();
		poller = Glib::signal_timeout().connect_seconds(
			[this](){poll(); return true;},
			1 /*s*/);
	}
}


void DigitalInput::poll()
{
	Value = input.getValue_isHigh() ? 1 : 0;
}
