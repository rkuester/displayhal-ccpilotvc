#ifndef INCLUDE_GLIBMM_H_INCLUDED
#define INCLUDE_GLIBMM_H_INCLUDED


#include <glibmm.h>


// Fix libsigc's interaction with C++ lambdas per
// https://mail.gnome.org/archives/libsigc-list/2012-January/msg00000.html

namespace sigc {

template <typename T_functor>
struct functor_trait<T_functor, false>
{
	typedef typename
	functor_trait<decltype(&T_functor::operator()), false>::result_type
	result_type;

	typedef T_functor functor_type;
};

}


#endif //INCLUDE_GLIBMM_H_INCLUDED
