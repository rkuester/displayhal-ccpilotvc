#ifndef BACKLIGHT_H_INCLUDED
#define BACKLIGHT_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal.Backlight1-adaptor.h"
#include "uielement.h"
#include "cc-aux.h"


class Backlight
: public org::pragmatux::DisplayHal::Backlight1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
, public UiElement
{
  public:
	Backlight(DBus::Connection&, ccaux::Backlight&, ccaux::FrontLed&);

  private:
	unsigned GetBrightness();
	void SetBrightness(const unsigned&);

	void enable();
	void disable();

	ccaux::Backlight &backlight;
	ccaux::FrontLed &led;
	unsigned intensity;
	bool enabled;
};


#endif // BACKLIGHT_H_INCLUDED
