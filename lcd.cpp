#include "lcd.h"
#include <fcntl.h>
#include <system_error>


int open_blank()
{
	int fd = ::open("/sys/class/graphics/fb0/blank", O_RDWR);
	if (fd < 0)
		throw std::system_error(fd, std::system_category());

	return fd;
}

void Lcd::enable()
{
	int fd = open_blank();
	::write(fd, "0", 1);
	::close(fd);
}


void Lcd::disable()
{
	int fd = open_blank();
	::write(fd, "1", 1);
	::close(fd);
}
