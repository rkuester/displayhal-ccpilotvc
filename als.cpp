#include "als.h"


ALS::ALS(DBus::Connection &c, ccaux::Lightsensor &s)
: DBus::ObjectAdaptor(c, "/org/pragmatux/DisplayHal/AmbientLightSensor")
, sensor(s)
{
	sensor.setOperatingRange(ccaux::Lightsensor::Range::Lux_4000);
	MaxIlluminanceRaw = 4000;
}


unsigned ALS::GetIlluminanceRaw()
{
	return sensor.getIlluminance();
}
