#ifndef LCD_H_INCLUDED
#define LCD_H_INCLUDED


#include "uielement.h"


class Lcd
: public UiElement
{
private:
	void enable();
	void disable();
};


#endif // LCD_H_INCLUDED
