## High-level notes

High-level notes

## Implementation notes

There are two layers: a hardware interface layer containing ccaux, which wraps
Maximatecc's hardware control library; and a layer of DBus objects, which
provide the DBus interface. The "business logic" is implemented in the DBus
objects, which directly access the hardware layer. As the HAL grows, it may be
more appropriate to create a third layer for the logic.
