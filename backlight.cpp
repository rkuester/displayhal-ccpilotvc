#include "backlight.h"


ccaux::FrontLed::Intensity led_intensity(unsigned backlight_intensity)
{
	return backlight_intensity * ccaux::FrontLed::MaxIntensity / 255;
}


Backlight::Backlight(DBus::Connection &c, ccaux::Backlight &b, ccaux::FrontLed &l)
: DBus::ObjectAdaptor(c, std::string("/org/pragmatux/DisplayHal/Backlight"))
, backlight(b)
, led(l)
, intensity(backlight.getIntensity())
, enabled(true)
{
	MaxBrightness = 255;
	led.setIntensity(led_intensity(intensity));
}


void Backlight::SetBrightness(const unsigned &b)
{
	if (b == 0)
		intensity = 1;
	else if (b > MaxBrightness())
		throw DBus::ErrorInvalidArgs("value exceeds MaxBrightness");
	else
		intensity = b;

	if (enabled) {
		backlight.setIntensity(intensity);
		led.setIntensity(led_intensity(intensity));
	}
}


unsigned Backlight::GetBrightness()
{
	return intensity;
}


void Backlight::enable()
{
	enabled = true;
	backlight.setIntensity(intensity);
	led.setIntensity(led_intensity(intensity));
}


void Backlight::disable()
{
	enabled = false;
	backlight.setIntensity(1);
	led.setIntensity(0);
}
