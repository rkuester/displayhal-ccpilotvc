#ifndef DISPLAYHAL_H_INCLUDED
#define DISPLAYHAL_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal1-adaptor.h"
#include "cc-aux.h"


class DisplayHal
: public org::pragmatux::DisplayHal1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	DisplayHal(DBus::Connection &c);

  private:
	ccaux::About about;
	ccaux::AuxVersion auxversion;
};


#endif // DISPLAYHAL_H_INCLUDE
