#ifndef ALS_H_INCLUDED
#define ALS_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal.AmbientLightSensor1-adaptor.h"
#include "cc-aux.h"


class ALS
: public org::pragmatux::DisplayHal::AmbientLightSensor1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public DBus::PropertiesAdaptor
{
  public:
	ALS(DBus::Connection&, ccaux::Lightsensor&);

  private:
	unsigned GetIlluminanceRaw();
	ccaux::Lightsensor &sensor;
};


#endif // ALS_H_INCLUDED
