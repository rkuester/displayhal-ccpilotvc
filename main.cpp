#include <dbus-c++/dbus.h>
#include <dbus-c++/glib-integration.h>
#include <unistd.h>
#include "als.h"
#include "displayhal.h"
#include "backlight.h"
#include "buzzer.h"
#include "digitalinput.h"
#include "digitaloutput.h"
#include "keyswitch.h"
#include "lcd.h"
#include "userinterface.h"
#include "cc-aux.h"
#include "include-glibmm.h"


int main()
{
	Glib::RefPtr<Glib::MainLoop> loop = Glib::MainLoop::create();

	ccaux::FrontLed hw_led;
	hw_led.setSignal(1, 100);

	ccaux::Config hw_config;
	hw_config.setOnOffSigAction(ccaux::Config::PowerAction::NoAction);
	hw_config.setPowerOnStartup(true);

	DBus::Glib::BusDispatcher dispatcher;
	DBus::default_dispatcher = &dispatcher;
	dispatcher.attach(0);
	DBus::Connection dbus = DBus::Connection::SystemBus();

	DisplayHal hal(dbus);

	Keyswitch keyswitch(dbus, hw_config);

	ccaux::Lightsensor hw_light;
	ALS dbus_als(dbus, hw_light);

	ccaux::Buzzer hw_buzzer;
	Buzzer dbus_buzzer(dbus, hw_buzzer);

	ccaux::CfgIn hw_in1(1);
	DigitalInput dbus_in1(dbus, hw_in1, "DigitalInput1");
	ccaux::CfgIn hw_in2(2);
	DigitalInput dbus_in2(dbus, hw_in2, "DigitalInput2");

	ccaux::PWMOut hw_out1(1);
	DigitalOutput dbus_out1(dbus, hw_out1, "DigitalOutput1");
	ccaux::PWMOut hw_out2(2);
	DigitalOutput dbus_out2(dbus, hw_out2, "DigitalOutput2");

	ccaux::Backlight hw_backlight;
	Backlight dbus_backlight{dbus, hw_backlight, hw_led};

	Lcd lcd;

	UserInterface dbus_ui(dbus);
	dbus_ui.add_element(dbus_buzzer);
	dbus_ui.add_element(lcd);
	dbus_ui.add_element(dbus_backlight);

	dbus.request_name("org.pragmatux.DisplayHal");
	loop->run();

	return 0;
}
