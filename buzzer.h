#ifndef BUZZER_H_INCLUDED
#define BUZZER_H_INCLUDED


#include "interfaces/org.pragmatux.DisplayHal.Buzzer1-adaptor.h"
#include "uielement.h"
#include "cc-aux.h"


class Buzzer
: public org::pragmatux::DisplayHal::Buzzer1_adaptor
, public DBus::ObjectAdaptor
, public DBus::IntrospectableAdaptor
, public UiElement
{
  public:
	Buzzer(DBus::Connection&, ccaux::Buzzer&);

  private:
	void On();
	void Off();

	ccaux::Buzzer &buzzer;
	bool enabled;

	void enable();
	void disable();
};


#endif // BUZZER_H_INCLUDED
